# Excel自动换行输入框

#### 介绍
用户输入一个数据回车一下即可（与扫码枪类似），由输入框控制Tab或Enter换行，也可同时进行正则检查。

#### 安装教程
如果要自己创建加载宏：
1.  打开空白Excel，点击文件>选项>信任中心>信任中心设置>宏设置，选中信任对VBA工程对象模型的访问。
2.  再次点击选项>自定义功能区，在右侧主选项卡中，选中开发工具。
3.  回到主窗口，点击开发工具标签，选择Visual Basic，插入用户窗体，并重命名为wh_IptForm，在窗体上画两个TextBox。
4.  查看该窗体的代码，粘贴wh_IptForm.txt的内容。
5.  查看ThisWorkbook的代码，添加open事件，内容为wh_IptForm.Show 0
6.  最后另存为xlam格式的加载宏。

#### 使用说明

1.  下载ipt.xlam文件，拖动该加载宏到需要输入内容的Excel窗口上，在显示的窗体上，输入一个数据回车一下即可。
2.  需要增减修改TextBox时，只需打开VB界面，在窗体代码内添加TextBox的Exit事件即可。

ps.  一共也没几行代码，凑合用吧 ^_^
